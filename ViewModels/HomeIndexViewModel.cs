﻿using OdeToFood.Models;
using System;
using System.Collections.Generic;

namespace OdeToFood.ViewModels
{
    public class HomeIndexViewModel
    {
        public IEnumerable<Restaurant> Restaurants { get; set;}
        public String CurretnMessage { get; set;}
    }
}
