﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdeToFood.controllers
{   
    // si posem controller com a nom, agafa el nom de la clase, ex: about
    // es pot posar random text tabe
    //action agafa el nom de l'accio
    [Route("company/[controller]/[action]")]
    public class AboutController
    {
        // /about/phone per defecte al controlador
       // [Route("")]
        public String Phone()
        {
            return "123456789";
        }
       
       // [Route("[action]")]
        public string Adress()
        {
            return "hello there";
        }
    }
}
