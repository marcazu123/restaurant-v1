﻿using Microsoft.AspNetCore.Mvc;
using OdeToFood.Models;
using OdeToFood.Services;
using OdeToFood.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdeToFood.controllers
{
    public class HomeController : Controller
     {
        private IRestaurantData _restarantData;
        private IGreeter _greeter;

        public HomeController(IRestaurantData restaurandata,
                               IGreeter greeter )
        {
            _restarantData = restaurandata;
            _greeter = greeter;
        }
        public IActionResult Index()
        {
            var model = new HomeIndexViewModel();
            model.Restaurants = _restarantData.GetAll();
            model.CurretnMessage = _greeter.GetMessageOfTheDay();
            // si no es declaren parametres busca la view que te el nom de la funcio ex: Index
            return View(model);
        }
        public IActionResult Details(int id)
        {
            var model = _restarantData.Get(id);
            //not found missatge per defecte
            if (model == null) return RedirectToAction(nameof(Index));
            return View(model);
            //retorna el contigut que li pasem ex: /home/Detauls/22 retorna 22
            //return Content(id.ToString());
        }
        // aixo si es un get
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        // aixo si es un post

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(RestaurantEditModel model)
        {
            if (ModelState.IsValid)
            {
                var newRestaurant = new Restaurant();
                newRestaurant.Name = model.Name;
                newRestaurant.Cuisine = model.Cuisine;
                newRestaurant = _restarantData.Add(newRestaurant);

                return RedirectToAction(nameof(Details), new { id = newRestaurant.Id });
            }
            else
            {
                return View();
            }
        }
        
    }
}