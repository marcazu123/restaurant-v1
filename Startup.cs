﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OdeToFood.Services;

namespace OdeToFood
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //s'ha de registrar els serveis creats

            //Singleton: si algú ho necesita es fa una unica crida i totes les parts fan servir la mateixa
            services.AddSingleton<IGreeter, Greeter>();
            //Scoped: cada cop que es necesita es crea una nova instacia i un cop feta la feina es tira
            services.AddSingleton<IRestaurantData, InMemoryRestaurantData>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            IGreeter greeter, ILogger<Startup> logger )
        {
            if (env.IsDevelopment())
            {
               // el env serveix per controlar qui utilitza la pagina
               // per exemple si ets developer i vols veure les excepcion 
               // o si ets un visitant i te la suda i si tens algun problema
               //et doni un mail per solucionar el problema

            app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler();
            }

            app.UseStaticFiles();

            app.UseMvc(ConfigureRoutes);

            //{
            //    return async context =>
            //    {
            //        logger.LogInformation("request in coming");
            //        if(context.Request.Path.StartsWithSegments("/my"))
            //        {
            //            await context.Response.WriteAsync("Hit!!");
            //            logger.LogInformation("Request handled");

            //        }
            //        else
            //        {
            //            await next(context);
            //            logger.LogInformation("request in outgoing");
            //        }
            //    };
            //});
            //app.UseWelcomePage(new WelcomePageOptions
            //{
            //    Path = "/wp"
            //});

            app.Run(async (context) =>
            {
                //pillar algo del configuration file aka appsetting.json
                // var greeting = Configuration["Greeting"];
                var greeting = greeter.GetMessageOfTheDay();
                context.Response.ContentType = "text/plain";
                await context.Response.WriteAsync($"Not found");
            });
        }

        private void ConfigureRoutes(IRouteBuilder routeBuilder)
        {
            //definim rurta per defecta al obrir navegador
            // /Home/Index com a ruta per defecte
            routeBuilder.MapRoute("Default", "{controller=Home}/{action=Index}/{id?}");
        }
    }
}
